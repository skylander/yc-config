yc-config
=========
A Java library built on top of [Java Simple Argument Parser (JSAP)](http://martiansoftware.com/jsap/) that allows the use of a default *"configuration file"* using the `--config-file` option.
You can view the Javadocs online at [http://skylander.bitbucket.org/yc-config/](http://skylander.bitbucket.org/yc-config/).

License
-------
yc-config is licensed under the Lesser GNU Public License. A copy of this license is available at [http://www.gnu.org/licenses/lgpl.html](http://www.gnu.org/licenses/lgpl.html).

Version 0.2 (3/3/2013)
----------------------
- Fixed bug where calling application does not know if '--help' was used.
- Added option for whether to exit after displaying usage information.
- Added StringParsers for key-value pairs.
- Added StringParsers for positive integers and doubles.


Version 0.1 (2012/3/26)
----------------------
- Initial release of yc-config library.
