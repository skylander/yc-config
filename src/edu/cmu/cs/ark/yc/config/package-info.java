/**
 * {@link edu.cmu.cs.ark.yc.config.AppConfig} is the main class for this library. See {@link edu.cmu.cs.ark.yc.config.AppConfig} and {@link edu.cmu.cs.ark.yc.config.DemoApp} for deatils.
 * 
 * @author Yanchuan Sim
 * @version 0.2
 */
package edu.cmu.cs.ark.yc.config;

