package edu.cmu.cs.ark.yc.config;

import java.util.AbstractMap.SimpleEntry;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;

import edu.cmu.cs.ark.yc.config.stringparsers.KeyValueParser;

/**
 * Demo app that demonstrates the new features implemented. Run with <code>-h</code> to see available parameters, and see source code on how to use the new implemented features.
 * 
 * @author Yanchuan Sim
 * @version 0.2
 */
public class DemoApp
{
  @SuppressWarnings("rawtypes")
  public static void main(String[] args) throws JSAPException
  {
    AppConfig app = new AppConfig("DemoApp", "App to demonstrate yc-config.", true, true);

    app.registerParameter(new FlaggedOption("input-docs", AppConfig.FILE_PARSER.setMustBeFile(true).setMustExist(true), AppConfig.NO_DEFAULT, true, AppConfig.NO_SHORTFLAG, "input-docs", "Read documents from file (space-separated words).").setUsageName("docs-file"));
    app.registerParameter(new FlaggedOption("output", AppConfig.FILE_PARSER.setMustExist(false), "output.sage", false, 'O', "output", "Write SAGE output here (default: output.sage).").setUsageName("sage-file"));
    app.registerParameter(new FlaggedOption("l1-weight", AppConfig.KEYVALUE_PARSER, AppConfig.NO_DEFAULT, false, '1', "l1-weight", "Set L1 regularization weight.").setAllowMultipleDeclarations(true).setUsageName("key=weight"));
    app.registerParameter(new FlaggedOption("iterations", AppConfig.POSITIVE_INTEGER_PARSER, "10", false, 'i', "iterations", "Number of iterations to optimize for (default: infinity).").setUsageName("N"));
    app.registerParameter(new Switch("overwrite", AppConfig.NO_SHORTFLAG, "overwrite", "Overwrite output file?"));

    JSAPResult result = app.parse(args);

    if (!result.success())
    {
      for (java.util.Iterator errs = result.getErrorMessageIterator(); errs.hasNext();)
        System.err.println("Error: " + errs.next());

      System.err.println("\nUsage: java " + DemoApp.class.getName());
      System.err.println("            " + app.getUsage() + "\n" + app.getHelp());
      System.exit(1);
    }

    System.out.println("Parameters passed in:");
    System.out.format("  input-docs %s\n", result.getFile("input-docs"));
    System.out.format("  output %s\n", result.getFile("input-docs"));
    System.out.format("  iterations %d\n", result.getInt("iterations"));
    System.out.format("  overwrite? %s\n", result.getBoolean("overwrite") ? "true" : "false");

    SimpleEntry<String, Double>[] weights = KeyValueParser.getKeyValueArray("l1-weight", result);
    for (SimpleEntry<String, Double> se : weights)
      System.out.format("  l1-weight %s = %f\n", se.getKey(), se.getValue());
  }
}
