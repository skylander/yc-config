package edu.cmu.cs.ark.yc.config.stringparsers;

import com.martiansoftware.jsap.ParseException;
import com.martiansoftware.jsap.StringParser;

/**
 * A {@link StringParser} extension for positive double arguments.
 * 
 * @author Yanchuan Sim
 * @version 0.2
 */
public class PositiveDoubleStringParser extends StringParser
{
  /*
   * (non-Javadoc)
   * @see com.martiansoftware.jsap.StringParser#parse(java.lang.String)
   */
  @Override
  public Object parse(String arg) throws ParseException
  {
    Double result = null;
    try
    {
      result = new Double(arg);
    }
    catch (NumberFormatException nfe)
    {
      throw new ParseException("Unable to convert '" + arg + "' to a Double.", nfe);
    }

    if (result <= 0)
      throw new ParseException("Value has to be greater than 0.0.");

    return result;
  }

}
