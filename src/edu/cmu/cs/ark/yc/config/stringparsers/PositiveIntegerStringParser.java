/**
 * 
 */
package edu.cmu.cs.ark.yc.config.stringparsers;

import com.martiansoftware.jsap.ParseException;
import com.martiansoftware.jsap.StringParser;

/**
 * A {@link StringParser} extension for positive integer arguments.
 * 
 * @author Yanchuan Sim
 * @version 0.2
 */
public class PositiveIntegerStringParser extends StringParser
{
  /*
   * (non-Javadoc)
   * @see com.martiansoftware.jsap.StringParser#parse(java.lang.String)
   */
  @Override
  public Object parse(String arg) throws ParseException
  {
    Integer result = null;
    try
    {
      result = Integer.decode(arg);
    }
    catch (NumberFormatException nfe)
    {
      throw new ParseException("Unable to convert '" + arg + "' to an Integer.", nfe);
    }

    if (result <= 0)
      throw new ParseException("Value has to be greater than 0.");

    return result;
  }

}
