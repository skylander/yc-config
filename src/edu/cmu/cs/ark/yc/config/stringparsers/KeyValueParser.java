package edu.cmu.cs.ark.yc.config.stringparsers;

import java.util.AbstractMap.SimpleEntry;

import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.ParseException;
import com.martiansoftware.jsap.StringParser;

/**
 * A {@link StringParser} extension for key-value pairs (key:value). Default delimiter is '='.
 * 
 * @author Yanchuan Sim
 * @version 0.2
 */
public class KeyValueParser extends com.martiansoftware.jsap.StringParser
{
  public String delimiter = "=";

  /**
   * Static method to extract key-value from {@link JSAPResult} as a {@link SimpleEntry}.
   * 
   * @param id
   *          ID of parameter.
   * @param result
   *          {@link JSAPResult} to get key-value pair from.
   * @return {@link SimpleEntry} type containing the key-value.
   */
  @SuppressWarnings("unchecked")
  public static SimpleEntry<String, Double> getKeyValue(String id, JSAPResult result)
  {
    return (SimpleEntry<String, Double>) result.getObject(id);
  }

  /**
   * Static method to extract key-value from {@link JSAPResult} as a {@link SimpleEntry} array.
   * 
   * @param id
   *          ID of parameter.
   * @param result
   *          {@link JSAPResult} to get key-value pairs from.
   * @return {@link SimpleEntry} array containing the key-value pairs.
   */
  @SuppressWarnings("unchecked")
  public static SimpleEntry<String, Double>[] getKeyValueArray(String id, JSAPResult result)
  {
    Object[] obj_arr = result.getObjectArray(id);

    @SuppressWarnings("rawtypes")
    SimpleEntry[] se_arr = new SimpleEntry[obj_arr.length];

    for (int i = 0; i < obj_arr.length; i++)
      se_arr[i] = (SimpleEntry<String, Double>) obj_arr[i];

    return se_arr;
  }

  /**
   * Creates a {@link KeyValueParser} with delimiter <code>delimiter</code>.
   * 
   * @param delimiter
   */
  public KeyValueParser(String delimiter)
  {
    setDelimiter(delimiter);
  }

  /**
   * Default {@link KeyValueParser} constructor.
   */
  public KeyValueParser()
  {
  }

  /*
   * (non-Javadoc)
   * @see com.martiansoftware.jsap.StringParser#parse(java.lang.String)
   */
  @Override
  public Object parse(String arg) throws ParseException
  {
    try
    {
      String[] fields = arg.split(delimiter, 2);

      SimpleEntry<String, Double> keyvalue = new SimpleEntry<String, Double>(fields[0], Double.parseDouble(fields[1]));

      return keyvalue;
    }
    catch (NumberFormatException nfe)
    {
      throw new ParseException("Unable to parse value in \"" + arg + "\".", nfe);
    }
    catch (Exception e)
    {
      throw new ParseException("Unable to parse key-value pair in \"" + arg + "\".", e);
    }
  }

  /**
   * Set the delimiter between <code>key</code> and <code>value</code>.
   * 
   * @param delimiter
   *          The delimiter.
   */
  public void setDelimiter(String delimiter)
  {
    this.delimiter = delimiter;
  }

}
